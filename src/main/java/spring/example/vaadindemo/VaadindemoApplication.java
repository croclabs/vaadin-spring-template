package spring.example.vaadindemo;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.event.EventListener;
import spring.example.vaadindemo.data.User;
import spring.example.vaadindemo.security.UserManager;
import spring.example.vaadindemo.security.UserPrincipal;

@SpringBootApplication
@Theme(value = "custom", variant = Lumo.DARK)
public class VaadindemoApplication extends SpringBootServletInitializer implements AppShellConfigurator {
	private @Autowired UserManager userManager;

	public static void main(String[] args) {
		SpringApplication.run(VaadindemoApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void createUsers() {
		User user = User.create();
		user.setUsername("user");
		user.setPassword("test");
		userManager.createUser(new UserPrincipal(user));

		User user2 = User.create();
		user2.setUsername("test");
		user2.setPassword("user");
		userManager.createUser(new UserPrincipal(user2));
	}
}
