package spring.example.vaadindemo.components;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import spring.example.vaadindemo.data.User;

public class AvatarComp extends HorizontalLayout {

    public AvatarComp(User user) {
        String name = user.getUsername() + " " + user.getUsername();
        Avatar avatarName = new Avatar(name);
        setAlignItems(Alignment.CENTER);
        add(avatarName, new Text(user.getUsername()));
    }
}
