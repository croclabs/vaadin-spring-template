package spring.example.vaadindemo.components;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WebBrowser;
import spring.example.vaadindemo.data.User;
import spring.example.vaadindemo.repos.UserRepo;
import spring.example.vaadindemo.security.SecurityService;

public class AvatarList extends HorizontalLayout {

    Tabs tabs = new Tabs();
    private final Messages content;

    public AvatarList(UserRepo userRepo, SecurityService security) {
        Iterable<User> users = userRepo.findAll();

        for (User user : users) {
            AvatarComp avatar = new AvatarComp(user);

            RouterLink link = new RouterLink();
            link.add(avatar);
            link.setTabIndex(-1);

            Tab tab = new Tab(link);

            // firefox does not evaluate padding correctly with these tabs!
            WebBrowser browser = VaadinSession.getCurrent().getBrowser();
            if (browser.isFirefox()) {
                tab.getStyle().set("padding-right", "30px");
            }

            tabs.add(tab);
        }

        //setPadding(false);

        content = new Messages();

        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.setHeightFull();
        tabs.getStyle().set("display", "flex");
        tabs.getStyle().set("border-right", "1px solid var(--lumo-contrast-10pct)");

        tabs.addSelectedChangeListener(
                event -> setContent(event.getSelectedTab()));
        this.add(tabs, content);
    }

    private void setContent(Tab tab) {
        content.removeAll();

        content.add(new Messages());
    }
}
