package spring.example.vaadindemo.components;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.messages.MessageInput;
import com.vaadin.flow.component.messages.MessageList;
import com.vaadin.flow.component.messages.MessageListItem;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class Messages extends Div {
    public Messages() {
        MessageList list = new MessageList();
        list.setHeightFull();
        MessageInput input = new MessageInput();
        input.setWidthFull();
        input.addSubmitListener(submitEvent -> {
            MessageListItem newMessage = new MessageListItem(
                    submitEvent.getValue(), Instant.now(), "");
            newMessage.setUserColorIndex(1);
            newMessage.addThemeNames("current-user");
            List<MessageListItem> items = new ArrayList<>(list.getItems());
            items.add(newMessage);
            list.setItems(items);
            UI.getCurrent().getPage().executeJs("arguments[0].scrollIntoView({behavior:'smooth'});", input);
        });

        MessageListItem message1 = new MessageListItem(
                "Nature does not hurry, yet everything gets accomplished.",
                LocalDateTime.now().minusDays(1).toInstant(ZoneOffset.UTC),
                "Matt Mambo");
        message1.setUserColorIndex(1);
        MessageListItem message2 = new MessageListItem(
                "Using your talent, hobby or profession in a way that makes you contribute with something good to this world is truly the way to go.",
                LocalDateTime.now().minusMinutes(55).toInstant(ZoneOffset.UTC),
                "Linsey Listy");
        message2.setUserColorIndex(1);
        list.setItems(message1, message2);

        VerticalLayout chatLayout = new VerticalLayout(list, input);
        chatLayout.expand(list);
        getStyle().set("overflow-y", "auto");
        add(chatLayout);
    }
}
