package spring.example.vaadindemo.data;

public class Contact {
    String firstName = "hehe";
    String lastName = "hoho";
    String email = "email";

    public String getStatus() {
        return "status";
    }

    public String getCompany() {
        return "company";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
