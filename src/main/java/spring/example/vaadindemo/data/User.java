package spring.example.vaadindemo.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Table("user_data")
public class User implements Persistable<UUID> {
    private User() {
        super();
    }

    @Id
    @Column
    private UUID id;

    @Column
    private String username;

    @Column
    private String password;

    @Transient
    private boolean fresh;

    public static User create() {
        return new User().setNew();
    }

    private User setNew() {
        this.fresh = true;
        return this;
    }

    public User id() {
        this.id = UUID.randomUUID();
        return this;
    }

    public UUID getId() {
        return id;
    }

    @Override
    public boolean isNew() {
        return fresh;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
