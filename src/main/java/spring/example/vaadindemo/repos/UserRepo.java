package spring.example.vaadindemo.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import spring.example.vaadindemo.data.User;

import java.util.UUID;

@Repository
public interface UserRepo extends CrudRepository<User, UUID> {
    User findByUsername(String username);

    boolean existsByUsername(String username);
}
