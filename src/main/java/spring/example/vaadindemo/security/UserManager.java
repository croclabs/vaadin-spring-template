package spring.example.vaadindemo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import spring.example.vaadindemo.data.User;
import spring.example.vaadindemo.repos.UserRepo;

@Service
public class UserManager implements UserDetailsManager, UserDetailsPasswordService {
    private @Autowired UserRepo userRepo;
    private @Autowired SecurityService service;

    private final PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Override
    public void createUser(UserDetails user) {
        UserPrincipal userp = (UserPrincipal) user;
        userp.getUser().id();

        if (userp.getUser().getId() == null || userRepo.existsById(userp.getUser().getId())) {
            createUser(user);
            return;
        }

        userp.getUser().setPassword(encoder.encode(userp.getPassword()));
        userRepo.save(userp.getUser());
    }

    @Override
    public void updateUser(UserDetails user) {
        UserPrincipal userp = (UserPrincipal) user;
        userRepo.save(userp.getUser());
    }

    @Override
    public void deleteUser(String username) {
        userRepo.delete(userRepo.findByUsername(username));
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        UserPrincipal userp = (UserPrincipal) service.getAuthenticatedUser();
        userp.getUser().setPassword(newPassword);
        userRepo.save(userp.getUser());
    }

    @Override
    public boolean userExists(String username) {
        return userRepo.existsByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return new UserPrincipal(user);
    }

    @Override
    public UserDetails updatePassword(UserDetails user, String newPassword) {
        UserPrincipal userp = (UserPrincipal) user;
        userp.getUser().setPassword(newPassword);
        userRepo.save(userp.getUser());
        return userp;
    }
}
