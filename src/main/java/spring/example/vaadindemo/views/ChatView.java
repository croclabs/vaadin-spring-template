package spring.example.vaadindemo.views;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import spring.example.vaadindemo.components.AvatarList;
import spring.example.vaadindemo.repos.UserRepo;
import spring.example.vaadindemo.security.SecurityService;

import javax.annotation.security.PermitAll;

@PermitAll
@Route(value = "chats", layout = MainLayout.class)
@PageTitle("Chats | Vaadin CRM")
public class ChatView extends HorizontalLayout {

    @Autowired
    public ChatView(UserRepo userRepo, SecurityService security) {
        setHeightFull();
        add(new AvatarList(userRepo, security));
    }
}
