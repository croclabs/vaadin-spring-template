package spring.example.vaadindemo.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.RouterLink;
import spring.example.vaadindemo.security.SecurityService;

import java.util.ArrayList;
import java.util.List;

public class MainLayout extends AppLayout implements BeforeEnterObserver {
    private final SecurityService securityService;

    Tabs tabs = null;

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        // TODO get navigation target by event and set selected tab this way
    }

    private enum MainTab {
        LIST(ListView.class, VaadinIcon.LIST, "Tasks"),
        DASHBOARD(DashboardView.class, VaadinIcon.DASHBOARD, "Dashboard"),
        CHATS(ChatView.class, VaadinIcon.CHAT, "Chats");

        MainTab(Class<? extends Component> clazz, VaadinIcon icon, String title) {
            this.clazz = clazz;
            this.icon = icon;
            this.title = title;
        }

        final Class<? extends Component> clazz;
        final VaadinIcon icon;
        final String title;
    }

    public MainLayout(SecurityService securityService) {
        this.securityService = securityService;

        DrawerToggle toggle = new DrawerToggle();

        H1 title = new H1("MyApp");
        title.getStyle().set("font-size", "var(--lumo-font-size-l)")
                .set("margin", "0");

        VerticalLayout logoutWrapper = new VerticalLayout();
        logoutWrapper.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        logoutWrapper.getStyle().set("flex-direction", "row");
        logoutWrapper.getStyle().set("padding", "0 16px");
        Button logout = new Button("Log out", e -> securityService.logout());
        logoutWrapper.add(logout);

        tabs = getTabs();
        addToDrawer(tabs);
        addToNavbar(toggle, title, logoutWrapper);
    }

    private Tabs getTabs() {
        Tabs tabs = new Tabs();
        tabs.add(createTabs());
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        return tabs;
    }

    private Tab[] createTabs() {
        List<Tab> tabs = new ArrayList<>();

        for (MainTab tab : MainTab.values()) {
            Icon icon = tab.icon.create();
            icon.getStyle().set("box-sizing", "border-box")
                    .set("margin-inline-end", "var(--lumo-space-m)")
                    .set("margin-inline-start", "var(--lumo-space-xs)")
                    .set("padding", "var(--lumo-space-xs)");

            RouterLink link = new RouterLink();
            link.setRoute(tab.clazz);
            link.add(icon, new Span(tab.title));
            link.setTabIndex(tab.ordinal());

            tabs.add(new Tab(link));
        }

        return tabs.toArray(new Tab[0]);
    }
}
