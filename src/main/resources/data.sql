CREATE TABLE user_data (
    id UUID NOT NULL PRIMARY KEY,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    constraint user_data_uk1 unique (username)
);